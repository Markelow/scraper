package scraper;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.*;

public class Scraper {
    
    final static Charset Encoding = StandardCharsets.UTF_8;
    final static String usage = "scraper url|file_with_urls words [-v] [-w] [-c] [-e]";
    final static String sentenceFirstPartPattern = "([A-Z][^.?!]*?)?(?<!\\w)(?i)(";
    final static String sentenceSecondPartPattern = ")(?!\\w)[^.?!]*?[.?!]{1,2}\"?";
    final static double nanoToSec = 1000000000;
    
    public static void main(String[] args) 
    {
        boolean vFlag = false, wFlag = false, cFlag = false, eFlag = false;
        String sentencePattern = null;
        String word, uri = null;
        List<String> urls = new ArrayList();
        String[] words = new String[0];
        String result;
        //process args
        switch (args.length)
        {
            case 0:
            case 1:
                System.out.println(usage);
                System.exit(1);
                break;
            default:
                uri = args[0];
                word = args[1];
                words = word.split(",");
                //process data processing comands
                if (2 < args.length)
                {
                    for (int i=0; i<args.length; ++i)
                    {
                        switch (args[i])
                        {
                            case "-v":
                                vFlag = true;
                                break;
                            case "-w":
                                wFlag = true;
                                break;
                            case "-c":
                                cFlag = true;
                                break;
                            case "-e":
                                eFlag = true;
                                break;
                        }
                    }
                }
        }
        //generate pattern for sentence extracting
        if (eFlag)
        {
            sentencePattern = sentenceFirstPartPattern;
            sentencePattern += words[0];
            for (int i=1; i<words.length; ++i)
            {
                sentencePattern += "|" + words[i];
            }
            sentencePattern += sentenceSecondPartPattern;
        }
        boolean canPrint = wFlag || cFlag || vFlag;
        try
        {
            //getting urls
            File file = new File(uri);
            if (file.exists() && !file.isDirectory())
            {
                urls = Files.readAllLines(Paths.get(uri), Encoding);
            }
            else
            {
                urls.add(uri);
            }
            //pasre pages
            Iterator<String> iterator = urls.iterator();
            URL url;
            BufferedReader in;
            String inputLine;
            int wordsCount, totalWordsCount = 0, charsCount, totalCharsCount = 0;
            long scrapingTime = 0, processingTime = 0, totalScrapingTime = 0, totalProcessingTime = 0;
            long startTime;
            Pattern pattern;
            String urlAdress;
            while(iterator.hasNext())
            {
                urlAdress = iterator.next();
                url =  new URL(urlAdress);
                in = new BufferedReader(new InputStreamReader(url.openStream()));
                wordsCount = 0;
                charsCount = 0;
                boolean isSentenceHeaderShown = false;
                startTime = System.nanoTime();
                while(null != (inputLine = in.readLine()))
                {
                    scrapingTime += System.nanoTime() - startTime;
                    //process data
                    startTime = System.nanoTime();
                    
                    charsCount += inputLine.length();
                    if (wFlag)
                    {
                        for (int i=0; i < words.length; ++i)
                        {
                            pattern = Pattern.compile(words[i]);
                            Matcher m = pattern.matcher(inputLine);
                            while (m.find())
                            {
                                wordsCount++;
                            }
                        }
                    }
                    if (eFlag && (0 != wordsCount))
                    {
                        if (!isSentenceHeaderShown)
                        {
                            System.out.println(urlAdress + ":" + " extracted senteces:");
                            isSentenceHeaderShown = true;
                        }
                        pattern = Pattern.compile(sentencePattern);
                        Matcher m = pattern.matcher(inputLine);
                        while (m.find())
                        {
                            System.out.println(m.group());
                        }
                    }
                    processingTime += System.nanoTime() - startTime;
                    startTime = System.nanoTime();
                }
                if (canPrint)
                {
                    result = "\t";
                    result += (cFlag) ? String.format("chars: %d \t", charsCount) : result;
                    result += (wFlag) ? String.format("words: %d \t", wordsCount) : result;
                    result += (vFlag) ? String.format("data scraping: %f s\t data processing: %f s", scrapingTime/nanoToSec, processingTime/nanoToSec) : result;
                    System.out.println(urlAdress + ":" + result);
                    
                    totalWordsCount += wordsCount;
                    totalCharsCount += charsCount;
                    totalScrapingTime += scrapingTime;
                    totalProcessingTime += processingTime;
                }
            }
            if (canPrint)
            {
                result = "\t";
                result += (cFlag) ? String.format("chars: %d \t", totalCharsCount) : result;
                result += (wFlag) ? String.format("words: %d \t", totalWordsCount) : result;
                result += (vFlag) ? String.format("data scraping: %f s\t data processing: %f s", totalScrapingTime/nanoToSec, totalProcessingTime/nanoToSec) : result;
                System.out.println("Total:" + result);
            }
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }    
}
